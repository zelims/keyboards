#include QMK_KEYBOARD_H
#if __has_include("keymap.h")
#    include "keymap.h"
#endif

// const rgblight_segment_t PROGMEM gaming_layer[] = RGBLIGHT_LAYER_SEGMENTS(
//     {11, 1, HSV_PURPLE}
//     {17, 3, HSV_PURPLE}
// );

/*
TEMPLATE:

KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_BSLS,
KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_PIPE,
KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,    KC_TRNS, KC_TRNS ,  KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                            KC_TRNS,    KC_TRNS,         KC_TRNS, KC_TRNS ,     KC_TRNS, KC_TRNS

*/

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT(
        QK_GESC, KC_1   , KC_2   , KC_3   , KC_4   , KC_5   ,                       KC_6   , KC_7   , KC_8   , KC_9   , KC_0   , KC_BSPC,
        KC_TAB , KC_Q   , KC_W   , KC_E   , KC_R   , KC_T   ,                       KC_Y   , KC_U   , KC_I   , KC_O   , KC_P   , KC_DEL ,
        KC_LSFT, KC_A   , KC_S   , KC_D   , KC_F   , KC_G   ,                       KC_H   , KC_J   , KC_K   , KC_L   , KC_SCLN, KC_QUOT,
        KC_LCTL, KC_Z   , KC_X   , KC_C   , KC_V   , KC_B   ,    KC_LALT, DF(9)  ,  KC_N   , KC_M   , KC_COMM, KC_DOT , KC_SLSH, KC_ENT,
                                    KC_LGUI,    MO(1),         KC_SPC , KC_ENT ,      MO(2), KC_RALT
    ),

    // Upper
    [1] = LAYOUT(
        KC_TILD, KC_F1  , KC_F2  , KC_F3  , KC_F4  , KC_F5  ,                        KC_F6  , KC_F7  , KC_F8  , KC_F9  , KC_F10 , KC_F11,
        KC_GRV , KC_F12 , KC_EQL , KC_MINS, KC_TRNS, QK_BOOT,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_UP  , KC_TRNS, KC_BSLS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_HOME, KC_TRNS, KC_LEFT, KC_DOWN, KC_RGHT, KC_PIPE,
        RGB_TOG, EE_CLR , KC_LGUI, KC_TRNS, KC_TRNS, KC_TRNS,    KC_TRNS, KC_TRNS ,  KC_END , KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
                                    KC_TRNS,    KC_TRNS,         KC_DEL , KC_DEL  ,     KC_TRNS, KC_TRNS
    ),

    // Lower
    [2] = LAYOUT(
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_MINUS, KC_EQUAL,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_UP  , KC_TRNS, KC_BSLS,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_HOME, KC_TRNS, KC_TRNS, KC_DOWN, KC_RGHT, KC_PIPE,
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,    KC_TRNS, KC_TRNS ,  KC_END , KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, QK_BOOT,
                                    KC_TRNS,    KC_TRNS,         KC_TRNS, KC_TRNS ,     KC_TRNS, KC_PSCR
    ),
    
    // Gaming
    [9] = LAYOUT(
        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,                        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
        KC_TRNS, KC_T   , KC_Q   , KC_W   , KC_E   , KC_R   ,                        KC_Y   , KC_U   , KC_I   , KC_O   , KC_P   , KC_DEL ,
        KC_TRNS, KC_G   , KC_A   , KC_S   , KC_D   , KC_F   ,                        KC_H   , KC_J   , KC_K   , KC_L   , KC_SCLN, KC_QUOT,
        KC_TRNS, KC_B   , KC_Z   , KC_X   , KC_C   , KC_V   ,    KC_LALT, DF(0)  ,   KC_N   , KC_M   , KC_COMM, KC_DOT , KC_SLSH, KC_ENT,
                                    KC_LGUI,    TL_LOWR,         KC_SPC , KC_ENT ,      TL_UPPR, KC_RALT
    )
};

bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    if (!rgb_matrix_indicators_user()) {
        return false;
    }

    for (uint8_t i = led_min; i < led_max; i++) {

        if ((i >= 28 && i <= 33) || (i >= 62 && i <= 67)) {
            // underglow
            RGB_MATRIX_INDICATOR_SET_COLOR(i, 50, 50, 50);
        }

        if (get_highest_layer(layer_state) == 9) {
            RGB_MATRIX_INDICATOR_SET_COLOR(11, 255, 0, 0); // e
            RGB_MATRIX_INDICATOR_SET_COLOR(17, 255, 0, 0); // s
            RGB_MATRIX_INDICATOR_SET_COLOR(18, 255, 0, 0); // d
            RGB_MATRIX_INDICATOR_SET_COLOR(19, 255, 0, 0); // f
        }
    }
    return true;
}

void suspend_power_down_kb(void)
{
    rgb_matrix_set_suspend_state(true);
}

void suspend_wakeup_init_kb(void)
{
    rgb_matrix_set_suspend_state(false);
}

/*
#ifdef RGB_MATRIX_ENABLE
bool rgb_matrix_indicators_advanced_user(uint8_t led_min, uint8_t led_max) {
    #if defined(GAMING_MODE)
        RGB_MATRIX_INDICATOR_SET_COLOR(11, 255, 0, 0); // e
        RGB_MATRIX_INDICATOR_SET_COLOR(17, 255, 0, 0); // s
        RGB_MATRIX_INDICATOR_SET_COLOR(18, 255, 0, 0); // d
        RGB_MATRIX_INDICATOR_SET_COLOR(19, 255, 0, 0); // f
    #endif
}
#endif // RGB_MATRIX_ENABLE
*/